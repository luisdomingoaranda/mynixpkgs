import os
from google.cloud import texttospeech
from .langs import LANGS
import random

class GoogleTTSClient:
    client = None

    @staticmethod
    def init(credentials):
        if credentials is None:
            raise Exception("Not found google gcloud credentials json")

        os.environ["GOOGLE_APPLICATION_CREDENTIALS"] = credentials
        GoogleTTSClient.client = texttospeech.TextToSpeechClient()


    @staticmethod
    def get_sound(phrase, language_code, voice_gender, filepath):
        if GoogleTTSClient.client is None:
            raise Exception("Google TTS client is not initialized. Please call GoogleTTSClient.init() first.")

        # Set the text input to be synthesized
        synthesis_input = texttospeech.SynthesisInput(text=phrase)

        # Build the voice request
        # Allow to pick a gender voice if any selected
        voices = []
        if voice_gender in ["", "null", None]:
            voices = LANGS[language_code]["voices"]
        else:
            voices = [voice for voice in LANGS[language_code]["voices"] if voice["gender"] == voice_gender]

        random_voice = random.choice(voices)
        voice = texttospeech.VoiceSelectionParams(
            language_code=random_voice["code"],
            name=random_voice["name"]
        )

        # Select the type of audio file you want returned
        audio_config = texttospeech.AudioConfig(
            audio_encoding=texttospeech.AudioEncoding.MP3
        )

        # Perform the text-to-speech request on the text input with the selected
        # voice parameters and audio file type
        response = GoogleTTSClient.client.synthesize_speech(
            input=synthesis_input, voice=voice, audio_config=audio_config
        )

        # Write mp3 file
        with open(filepath, "wb") as out:
            # Write the response to the output file.
            out.write(response.audio_content)
            print(f"Audio content written to file '{filepath}'")

