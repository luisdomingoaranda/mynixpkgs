# Supported voices: https://cloud.google.com/text-to-speech/docs/voices
LANGS = {
    "ru": {
        "voices": [
            {
                "name": "ru-RU-Wavenet-A",
                "code": "ru-RU",
                "gender": "female"
            },
            {
                "name": "ru-RU-Wavenet-B",
                "code": "ru-RU",
                "gender": "male"
            },
            {
                "name": "ru-RU-Wavenet-C",
                "code": "ru-RU",
                "gender": "female"
            },
            {
                "name": "ru-RU-Wavenet-D",
                "code": "ru-RU",
                "gender": "male"
            },
            {
                "name": "ru-RU-Wavenet-E",
                "code": "ru-RU",
                "gender": "female"
            },
        ]
    },
    "en": {
        "voices": [
            {
                "name": "en-GB-Wavenet-A",
                "code": "en-GB",
                "gender": "female"
            },
            {
                "name": "en-GB-Wavenet-B",
                "code": "en-GB",
                "gender": "male"
            },
            {
                "name": "en-GB-Wavenet-C",
                "code": "en-GB",
                "gender": "female"
            },
            {
                "name": "en-GB-Wavenet-D",
                "code": "en-GB",
                "gender": "male"
            },
            {
                "name": "en-GB-Wavenet-F",
                "code": "en-GB",
                "gender": "female"
            },
            {
                "name": "en-US-Wavenet-A",
                "code": "en-US",
                "gender": "male"
            },
            {
                "name": "en-US-Wavenet-B",
                "code": "en-US",
                "gender": "male"
            },
            {
                "name": "en-US-Wavenet-C",
                "code": "en-US",
                "gender": "female"
            },
            {
                "name": "en-US-Wavenet-D",
                "code": "en-US",
                "gender": "male"
            },
            {
                "name": "en-US-Wavenet-E",
                "code": "en-US",
                "gender": "female"
            },
        ]
    },
    "pt": {
        "voices": [
            {
                "name": "pt-BR-Wavenet-A",
                "code": "pt-BR",
                "gender": "female"
            },
            {
                "name": "pt-PT-Wavenet-A",
                "code": "pt-PT",
                "gender": "female"
            },
            {
                "name": "pt-PT-Wavenet-B",
                "code": "pt-PT",
                "gender": "male"
            },
            {
                "name": "pt-PT-Wavenet-C",
                "code": "pt-PT",
                "gender": "male"
            },
            {
                "name": "pt-PT-Wavenet-D",
                "code": "pt-PT",
                "gender": "female"
            },
        ]
    },
    "de": {
        "voices": [
            {
                "name": "de-DE-Wavenet-A",
                "code": "de-DE",
                "gender": "female"
            },
            {
                "name": "de-DE-Wavenet-B",
                "code": "de-DE",
                "gender": "male"
            },
            {
                "name": "de-DE-Wavenet-C",
                "code": "de-DE",
                "gender": "female"
            },
            {
                "name": "de-DE-Wavenet-D",
                "code": "de-DE",
                "gender": "male"
            },
            {
                "name": "de-DE-Wavenet-E",
                "code": "de-DE",
                "gender": "male"
            },
            {
                "name": "de-DE-Wavenet-F",
                "code": "de-DE",
                "gender": "female"
            },
        ]
    },
    "cmn": {
        "voices": [
            {
                "name": "cmn-CN-Wavenet-A",
                "code": "cmn-CN",
                "gender": "female"
            },
            {
                "name": "cmn-CN-Wavenet-B",
                "code": "cmn-CN",
                "gender": "male"
            },
            {
                "name": "cmn-CN-Wavenet-C",
                "code": "cmn-CN",
                "gender": "male"
            },
            {
                "name": "cmn-CN-Wavenet-D",
                "code": "cmn-CN",
                "gender": "female"
            },
            {
                "name": "cmn-TW-Wavenet-A",
                "code": "cmn-TW",
                "gender": "female"
            },
            {
                "name": "cmn-TW-Wavenet-B",
                "code": "cmn-TW",
                "gender": "male"
            },
            {
                "name": "cmn-TW-Wavenet-C",
                "code": "cmn-TW",
                "gender": "male"
            },
        ]
    },
}
