from typing import TypedDict, List, Literal

class Voice(TypedDict):
    name: Literal["alloy", "echo", "fable", "onyx", "nova", "shimmer"]
    gender: Literal["male", "female"]

VOICES: List[Voice] = [
    {
        "name": "alloy",
        "gender": "female"
    },
    {
        "name": "echo",
        "gender": "male"
    },
    {
        "name": "fable",
        "gender": "female"
    },
    {
        "name": "onyx",
        "gender": "male"
    },
    {
        "name": "nova",
        "gender": "female"
    },
    {
        "name": "shimmer",
        "gender": "female"
    }
]
