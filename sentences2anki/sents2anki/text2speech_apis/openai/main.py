import random
from openai import OpenAI
from .voices import VOICES

class OpenAITTSClient:
    client = None

    @staticmethod
    def init(api_key):
        if api_key is None:
            raise Exception("Not found OPENAI_API_KEY env variable")

        OpenAITTSClient.client = OpenAI(api_key=api_key)

    @staticmethod
    def get_sound(phrase, voice_gender, filepath):
        # Ensure that the client is initialized
        if OpenAITTSClient.client is None:
            raise Exception("OpenAI TTS client is not initialized. Please call OpenAITTSClient.init() first.")

        # Get random voice with proper gender

        # Build the voice request
        # Allow to pick a gender voice if any selected
        voices = []
        if voice_gender in ["", "null", None]:
            voices = VOICES
        else:
            voices = [voice for voice in VOICES if voice["gender"] == voice_gender]

        random_voice = random.choice(voices)

        response = OpenAITTSClient.client.audio.speech.create(
            model="tts-1",
            voice=random_voice["name"],
            input=phrase
        )

        response.stream_to_file(filepath)

        return
