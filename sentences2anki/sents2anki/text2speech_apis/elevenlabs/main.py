import requests
import random
import json

class ElevenLabsTTSClient:
    api_key = None
    voices = []

    @staticmethod
    def init(api_key, voices_json):
        if api_key is None:
            raise Exception("Not found ELEVENLABS_API_KEY env variable")

        ElevenLabsTTSClient.api_key = api_key

        with open(voices_json, 'r') as file:
            ElevenLabsTTSClient.voices = json.load(file)["voices"]


    @staticmethod
    def get_sound(phrase, voice_gender, filepath):
        # Ensure that the client is initialized
        if ElevenLabsTTSClient.api_key is None:
            raise Exception("ElevenLabs TTS client is not initialized. Please call ElevenLabsTTSClient.init() first.")

        headers = {
          "Accept": "audio/mpeg",
          "Content-Type": "application/json",
          "xi-api-key": ElevenLabsTTSClient.api_key
        }

        data = {
          "text": phrase,
          "model_id": "eleven_multilingual_v2",
          "voice_settings": {
            "stability": 0.5,
            "similarity_boost": 0.5
          }
        }

        # Select random voice with proper gender
        # Allow to pick a gender voice if any selected
        voices = []
        if voice_gender in ["", "null", None]:
            voices = ElevenLabsTTSClient.voices
        else:
            voices = [voice for voice in ElevenLabsTTSClient.voices if voice["gender"] == voice_gender]

        random_voice = random.choice(voices)
        url = f"https://api.elevenlabs.io/v1/text-to-speech/{random_voice['id']}"

        response = requests.post(url, json=data, headers=headers)
        with open(f'{filepath}', 'wb') as f:
            for chunk in response.iter_content(chunk_size=1024):
                if chunk:
                    f.write(chunk)

        return
