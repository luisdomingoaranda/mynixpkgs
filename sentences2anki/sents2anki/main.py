import random
import csv
import os
import shutil
import argparse
import genanki
from datetime import datetime
from anki.storage import Collection
from anki.importing.apkg import AnkiPackageImporter
from sents2anki.model import MODEL_FULL, MODEL_WITHOUT_TRANSLATION_CARDS
from sents2anki.text2speech_apis.openai.main import OpenAITTSClient
from sents2anki.text2speech_apis.elevenlabs.main import ElevenLabsTTSClient
from sents2anki.text2speech_apis.google.main import GoogleTTSClient
from dotenv import load_dotenv

# Generate a timestamp to get a unique export .apkg
timestamp = datetime.now().strftime("%Y%m%d_%H%M%S")

PATHS = {
    "export_filename": os.path.abspath(f"exports_tmp_{timestamp}.apkg"),
}


def list_full_paths(directory):
    return [os.path.join(directory, file) for file in os.listdir(directory)]


def remove_forbidden_media_chars(string):
    # Anki does not allow certain characters present in media filenames
    forbidden_chars = [
        "?",
        ":",
        '"'
    ]

    for forbidden_char in forbidden_chars:
        string = string.replace(forbidden_char, "")

    return string


def get_sound(phrase, language_code, voice_gender, text2speech_api):
    phrase = remove_forbidden_media_chars(phrase)
    filename = f"{phrase}.mp3"
    filepath = f"{PATHS['tmp_media_folder']}/{filename}"

    if text2speech_api == "google":
        GoogleTTSClient.get_sound(phrase, language_code, voice_gender, filepath)
    elif text2speech_api == "openai":
        OpenAITTSClient.get_sound(phrase, voice_gender, filepath)
    elif text2speech_api == "elevenlabs":
        ElevenLabsTTSClient.get_sound(phrase, voice_gender, filepath)
    elif text2speech_api == "any":
        # List of available TTS client functions
        tts_clients = [
            lambda: GoogleTTSClient.get_sound(phrase, language_code, voice_gender, filepath),
            lambda: OpenAITTSClient.get_sound(phrase, voice_gender, filepath),
            lambda: ElevenLabsTTSClient.get_sound(phrase, voice_gender, filepath)
        ]
        # Randomly select and call one of the TTS client functions
        random.choice(tts_clients)()

    return f"[sound:{filename}]"

def get_note_fields(row, target_language_code, text2speech_api):
    voice_gender = row[1].lower()
    source_sentence = row[2]
    sentence_translation = row[3]
    sound = get_sound(sentence_translation, target_language_code, voice_gender, text2speech_api)

    return [
        voice_gender,
        source_sentence,
        sound,
        sentence_translation,
        ""  # extra field
    ]


def add_note(deck, model, row, target_language_code, text2speech_api):
    my_note = genanki.Note(
        model=model,
        fields=get_note_fields(row, target_language_code, text2speech_api)
    )
    deck.add_note(my_note)


def get_args():
    parser = argparse.ArgumentParser(
        description="Create anki notes for language learning"
    )
    parser.add_argument(
        "--file",
        required=True,
        type=str,
        help="File with language sentences pairs",
        metavar=""
    )
    parser.add_argument(
        "--text2speech_api",
        required=True,
        type=str,
        choices=["openai", "elevenlabs", "google", "any"],
        help="Choose the text-to-speech API: openai, elevenlabs, or google, or any of them",
        metavar=""
    )
    parser.add_argument(
        "--anki_collection",
        default="",
        type=str,
        help="Path to anki user collection (e.g. '/home/user/.local/share/Anki2/User 1/collection.anki2')",
        metavar=""
    )
    parser.add_argument(
        "--without_translation_cards",
        action="store_true",
        help="Set this flag to exclude translations type cards (from source to target language)."
    )

    # Parse known arguments to check the value of --text2speech_api
    args, unknown = parser.parse_known_args()
    if args.text2speech_api == "google":
        parser.add_argument(
            "--credentials",
            required=True,
            type=str,
            help="Path to google cloud json credentials to text-to-speech-service",
            metavar=""
        )
    elif args.text2speech_api == "openai":
        parser.add_argument(
            "--env_file",
            required=True,
            type=str,
            help="Path to .env file with 'OPENAI_API_KEY' environment variable",
            metavar=""
        )
    elif args.text2speech_api == "elevenlabs":
        parser.add_argument(
            "--env_file",
            required=True,
            type=str,
            help="Path to .env file with 'ELEVENLABS_API_KEY' environment variable",
            metavar=""
        )
        parser.add_argument(
            "--voices_json",
            required=True,
            type=str,
            help="json file with voices to use",
            metavar=""
        )
    elif args.text2speech_api == "any":
        parser.add_argument(
            "--credentials",
            required=True,
            type=str,
            help="Path to google cloud json credentials to text-to-speech-service",
            metavar=""
        )
        parser.add_argument(
            "--env_file",
            required=True,
            type=str,
            help="Path to .env file with 'ELEVENLABS_API_KEY' environment variable",
            metavar=""
        )
        parser.add_argument(
            "--voices_json",
            required=True,
            type=str,
            help="json file with voices to use",
            metavar=""
        )

    # Parse arguments again to include the conditionally added argument
    args = parser.parse_args()
    return args


def get_inputs():
    args = get_args()

    if args.text2speech_api == "google":
        GoogleTTSClient.init(args.credentials)
    elif args.text2speech_api == "openai":
        load_dotenv(args.env_file) # Load environment variables from .env file
        OpenAITTSClient.init(os.environ.get("OPENAI_API_KEY"))
    elif args.text2speech_api == "elevenlabs":
        load_dotenv(args.env_file) # Load environment variables from .env file
        ElevenLabsTTSClient.init(os.environ.get("ELEVENLABS_API_KEY"), args.voices_json)
    elif args.text2speech_api == "any":
        load_dotenv(args.env_file) # Load environment variables from .env file
        GoogleTTSClient.init(args.credentials)
        OpenAITTSClient.init(os.environ.get("OPENAI_API_KEY"))
        ElevenLabsTTSClient.init(os.environ.get("ELEVENLABS_API_KEY"), args.voices_json)


    # Get deck name, deck id and language code from input file
    deck_name = args.file.split(".")[0]
    deck_id = int(args.file.split(".")[1])
    target_language_code = args.file.split(".")[2]

    return (
        args.anki_collection,
        args.file,
        target_language_code,
        deck_name,
        deck_id,
        args.text2speech_api,
        args.without_translation_cards
    )


def run_main():

    # Parse inputs
    (
    anki_collection,
    file,
    target_language_code,
    deck_name,
    deck_id,
    text2speech_api,
    without_translation_cards
    ) = get_inputs()

    # Temporal folder with media
    PATHS["tmp_media_folder"] = "./.tmp.media"
    if not os.path.exists(PATHS["tmp_media_folder"]):
        os.mkdir(PATHS["tmp_media_folder"])

    # Create genanki deck
    my_deck = genanki.Deck(deck_id, deck_name)

    my_model = None
    if without_translation_cards:
        my_model = MODEL_WITHOUT_TRANSLATION_CARDS
    else:
        my_model = MODEL_FULL

    # Read from file
    with open(file, "r") as csvfile:
        csv_reader = csv.reader(csvfile, delimiter=',')
        for row in csv_reader:
            # Skip rows where the first field is "x"
            if row[0].lower() == "x":
                continue

            # Add note to deck
            add_note(
                my_deck,
                my_model,
                row,
                target_language_code,
                text2speech_api
            )
            print(f"Added notes for: {row[2]}")

    print("-------------------------------------------------------------------")

    # Create .apkg with notes and media
    export_filename = PATHS["export_filename"]
    print(f"exported to: {export_filename}")
    my_package = genanki.Package(my_deck)
    my_package.media_files = list_full_paths(PATHS["tmp_media_folder"])
    my_package.write_to_file(export_filename)
    # Remove .apkg media folder with all its files (rm -rf)
    shutil.rmtree(PATHS["tmp_media_folder"])

    print("-------------------------------------------------------------------")

    # if path to anki collection was given as an argument
    # Import the created .apkg into your local anki collection
    if anki_collection:
        # Import apkg to anki
        collection = Collection(anki_collection)
        importer = AnkiPackageImporter(collection, export_filename)
        importer.run()
        print("Notes imported to anki")
        # Remove exported apkg file
        os.remove(PATHS["export_filename"])
