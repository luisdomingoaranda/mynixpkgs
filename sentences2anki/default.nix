{ pkgs ? import <nixpkgs> { } }:

let
  pythonPkgs = pkgs.python3Packages;
  customOpenai = pythonPkgs.openai.overridePythonAttrs (oldAttrs: rec {
    doCheck = false;
  });
in
pythonPkgs.buildPythonApplication {
  pname = "sentences2anki";
  version = "1.0";

  src = ./.;

  propagatedBuildInputs = with pythonPkgs; [
    pip
    genanki
    google-cloud-texttospeech
    customOpenai
    python-dotenv
    requests
  ] ++ [
    pkgs.anki
  ];

  doCheck = true;
}
