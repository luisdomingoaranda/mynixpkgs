# Create anki notes for language learning.

Either output an `.apkg` file that you can import in Anki.
Or import the notes directly into your existing Anki collection.

```
sentences2anki --help
```

# Sentences csv file

Create a new file with the following structure

```
$ touch <deck_name>.<deck_id>.<target_language_code>.csv
```

e.g.

```
$ touch russian.2627825192973.ru.csv
```

If you still don't have a deck, you can put whatever name you like
and a random 13 digit number as id.

If you want to add notes to an already existing deck, you need to check its
name and `deck_id` by inspecting the sqlite database

```
$ sqlitebrowser collection.anki2
```

The contents of the csv file, for each row should be like:

- In the field field, you can write a "x" character to avoid that line being processed.
Imagine you already have that sentece loaded into anki, but you wanna preserve it in written form in the csv for future reference.

- The second field is optional, and specifies the required voice gender ("male", "female", or blank values).

- The third field is the sentence in the source language.

- The fourth field is the sentence in your target language, whatever you're trying to learn.

- Strings can be entered inside quotes ("<text>") or not, but if the sentences have comma's inside them, it's required to use them.
Since this is a csv file, where fields are separated by comma's.

- Avoid nesting double quotes! Single quotes inside double quotes are fine.
- Don't add spaces after csv comma delimiters.

e.g.

```
x,,"Como se dice 'internet' en ruso?","Как cказать 'Internet' по-русски?"
x,female,"I would like to order a cup of coffee","Я бы хотела заказать чашечку кофе"
x,male,"I wanted to watch the film 'Avatar'","Я хоте́л смотре́ть фильм 'Авата́р'"
```


# Command execution

In general you should provide the before mentioned csv file.

A Secret/token to a text-to-speech api (openai, elevenlabs, or google).

Optionally a path to your anki user collection. If given, the .apkg file
will be imported into your anki collection.

And you can also skip the generation of translation notes (from source to target language), with
the `--without_translation_cards` flag.

Currently, you can query the google, openai and elevenlabs api's to get the sound of your target language phrases.

- Google

Google API requires a `gcloud_service_account_credentials.json` file.

```
$ sentences2anki \
  --text2speech_api google
  --credentials './gcloud_service_account_credentials.json' \
  --file russian.2627825192973.ru.csv \
  [--without_translation_cards]
  [--anki_collection '/home/username/.local/share/Anki2/User 1']
```

- OpenAI

OpenAI api requires an access token.

Write your openai access token as an environment variable in some file (e.g. .env).
The environment variable name should be "OPENAI_API_TOKEN".

e.g.

```
OPENAI_API_KEY=sk-4JlwG20DcE8oOwFNnVc0001122FJjAVdrFrx5iydj8KOqBYt
```

```
$ sentences2anki \
  --text2speech_api openai
  --env_file './.env' \
  --file russian.2627825192973.ru.csv \
  [--without_translation_cards]
  [--anki_collection '/home/username/.local/share/Anki2/User 1']
```

- Elevenlabs

Elevenlabs api requires an access token.

write your elevenlabs access token as an environment variable in some file (e.g. .env).
The environment variable name should be "ELEVENLABS_API_KEY".

e.g.

```
ELEVENLABS_API_KEY=ce4238e2f12230a0000b5b1ca797eff8
```

Additionaly, since elevenlabs allows you to use your custom voices, you should create an additional file
in json format with your voices id's and gender, such as

```
{
  "voices": [
    {
      "id": "D5C88iHpjWDDtDd8YSNB",
      "name": "Putin",
      "gender": "male"
    },
    {
      "id": "7xJI9Z5ebr8BAEErvulG",
      "name": "Khabib Nurmagomedov",
      "gender": "male"
    },
    {
      "id": "vtdKxWOFmOOLVt224sdi",
      "name": "x2ded",
      "gender": "male"
    },
    {
      "id": "Wo8B7bH6EHCIMcwQ4ZDH",
      "name": "katya adushkina",
      "gender": "female"
    },
    {
      "id": "npVMWDEiueBt1VmUOwxY",
      "name": "babushka with flag",
      "gender": "female"
    },
    {
      "id": "41CEEt29I5zDW0RTjcvo",
      "name": "female russian storyteller",
      "gender": "female"
    }
  ]
}
```

To finally call the command like

```
$ sentences2anki \
  --text2speech_api elevenlabs
  --env_file ./.env
  --voices_json ./elevenlabs_voices.json
  --file russian_prueba.131313.ru.csv
  [--without_translation_cards]
  [--anki_collection '/home/username/.local/share/Anki2/User 1']
```

- Any

To get sounds from any supported API (randomly selected), you'll need
to provide all the API keys and credentials, along with any other required files or flags.

```
$ sentences2anki \
  --text2speech_api any
  --credentials './gcloud_service_account_credentials.json' \
  --env_file ./.env
  --voices_json ./elevenlabs_voices.json
  --file russian_prueba.131313.ru.csv
  [--without_translation_cards]
  [--anki_collection '/home/username/.local/share/Anki2/User 1']
```
