{ pkgs ? import <nixpkgs> }:
with pkgs;

derivation {
  name = "simple";
  builder = "${bash}/bin/bash";
  args = [ ./simple_builder.sh ];
  inherit gcc coreutils;
  src = ./simple.c;
  system = "x86_64-linux";
}
