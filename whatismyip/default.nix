{ pkgs ? import <nixpkgs> {} }:

pkgs.writeShellScriptBin "whatismyip" ''
    ${pkgs.curl}/bin/curl http://httpbin.org/get \
      | ${pkgs.jq}/bin/jq --raw-output .origin
''
