{ pkgs ? import <nixpkgs> {} }:

let
  csdPost = pkgs.writeShellApplication {
    name = "csdPost";
    text = builtins.readFile ./csd-post.sh;
    runtimeInputs = [ pkgs.curl pkgs.xmlstarlet ];
    checkPhase = false;
  };
in
pkgs.writeShellScriptBin "vpn_rsi" ''
    sudo ${pkgs.openconnect}/bin/openconnect \
      vpn.ruralserviciosinformaticos.com \
      --authgroup="RSI (OTP)" \
      --user=u028693 \
      --csd-wrapper ${csdPost}
''
