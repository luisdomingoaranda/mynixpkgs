{ pkgs ? import <nixpkgs> {} }:
with pkgs;

stdenv.mkDerivation rec {
  pname = "emojify";
  version = "2.0.0";

  # Using this build support function to fetch it from github
  src = fetchFromGitHub {
    owner = "mrowa44";
    repo = "emojify";
    # The git tag to fetch
    rev = "${version}";
    # Hashes must be specified so that the build is purely functional
    sha256 = "0zhbfxabgllpq3sy0pj5mm79l24vj1z10kyajc4n39yq8ibhq66j";
  };

  # We override the install phase, as the emojify project doesn't use make
  installPhase = ''
    # Make the output directory
    mkdir -p $out/bin

    # Copy the script there and make it executable
    cp emojify $out/bin/
    chmod +x $out/bin/emojify
  '';
}
