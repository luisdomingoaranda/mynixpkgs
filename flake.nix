{
  description = "My nix packages";

  inputs = {
    nixpkgs = {
      url = "github:nixos/nixpkgs/nixos-unstable";
      # url = "github:nixos/nixpkgs/nixos-22.05";
    };
  };

  outputs = inputs @ { self, nixpkgs, ... }:
    let
      pkgs = import nixpkgs {
      system = "x86_64-linux";
      config.allowUnfree = true;
    };

    in
    {
      # Use locally: "nix build .#<packageName>"
      # Use from flake: "mynixpkgs.packages.x86_64-linux.<packageName>"
      packages.x86_64-linux = import ./all-my-packages.nix { inherit pkgs; };
      packages.aarch64-linux = import ./all-my-packages.nix { inherit pkgs; };
    };
}
