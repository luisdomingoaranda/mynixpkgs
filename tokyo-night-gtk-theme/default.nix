{ lib
, stdenv
, fetchFromGitHub
}:

let
  pname = "Tokyo-Night-GTK-Theme";
in

stdenv.mkDerivation rec {
  inherit pname;
  version = "2022-08-29";

  src = fetchFromGitHub {
    owner = "Fausto-Korpsvart";
    repo = pname;
    rev = "f7ae3421ac0d415ca57fb6224e093e12b8a980bb";
    sha256 = "sha256-90V55pRfgiaP1huhD+3456ziJ2EU24iNQHt5Ro+g+M0=";
  };

  installPhase = ''
    mkdir -p $out/share/themes
    mkdir -p $out/share/icons
    cp -r ./themes/* $out/share/themes
    cp -r ./icons/Tokyonight-Light-Alt $out/share/icons
  '';

  meta = with lib; {
    description = "Tokyo-Night-GTK-Theme";
    homepage = "https://github.com/Fausto-Korpsvart/Tokyo-Night-GTK-Theme";
    license = licenses.gpl3Only;
    platforms = platforms.unix;
    maintainers = [ "offline" ];
  };
}
