{ pkgs ? import <nixpkgs> {} }:
with pkgs;

{
  # niv-env -iA whatismyip --file ./all-my-packages.nix
  whatismyip = callPackage ./whatismyip {};
  simple = callPackage ./simple {};
  emojify = callPackage ./emojify {};
  kingstvis = callPackage ./kingstvis {};
  sentences2anki = callPackage ./sentences2anki {};
  vpn_rsi = callPackage ./vpn_rsi {};
  tokyo-night-gtk-theme = callPackage ./tokyo-night-gtk-theme {};
  Simp1e-Tokyo-Night = callPackage ./Simp1e-Tokyo-Night {};
}

