# My nixpkgs

Flake with my "private" nix packages


# Usage

### With nix command

```
# Locally
cd mynixpkgs
nix run .#<packageName>
nix build .#<packageName>
nix develop .#<packageName>

# Or remotely
nix run gitlab:luisdomingoaranda/mynixpkgs#<packageName>
nix build gitlab:luisdomingoaranda/mynixpkgs#<packageName>
nix develop gitlab:luisdomingoaranda/mynixpkgs#<packageName>
```


### From other flakes

Within inputs section
```
mynixpkgs = {
  url = "gitlab:luisdomingoaranda/mynixpkgs";
  inputs.nixpkgs.follows = "nixpkgs-unstable";
};
```

Then to access the packages
```
# mynixpkgs.packages.<system>.<packageName>
mynixpkgs.packages.x86_64-linux.whatismyip
```
