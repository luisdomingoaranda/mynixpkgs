{ lib
, stdenv
}:

stdenv.mkDerivation rec {
  pname = "Simp1e-Tokyo-Night";
  version = "2023-02-09";

  src = builtins.fetchTarball {
    url = "https://gitlab.com/cursors/simp1e/-/jobs/3719462594/artifacts/raw/built_themes/Simp1e-Tokyo-Night.tgz";
    sha256 = "0ml5ccmiqwhgxl1agzxy83d3p9d5626b04v6pqz6hqlx2h87l80g";
  };

  installPhase = ''
    mkdir -p $out/share/icons/Simp1e-Tokyo-Night
    cp -r * $out/share/icons/Simp1e-Tokyo-Night
  '';

  meta = with lib; {
    description = "Simp1e-Tokyo-Night cursor variant";
    homepage = "https://gitlab.com/cursors/simp1e";
    license = licenses.gpl3Only;
    platforms = platforms.unix;
    maintainers = [ "offline" ];
  };
}
